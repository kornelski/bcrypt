# JavaScript bCrypt

This is a fork of [javascript-bcrypt](https://github.com/nevins-b/javascript-bcrypt)
optimized for speed in modern browsers.

Currently it's 3× slower than native in Firefox.
